# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.1 (2021-05-17)

### Edited

* Build feature hierarchy.

### Fixed

* CDS turned into pseudo (interupted by a gap or chromosome end): NCAS_0B00100.
* CDS turned into pseudo (interupted by a gap or chromosome end): NCAS_0C00100.
* CDS turned into pseudo (interupted by a gap or chromosome end): NCAS_0D00100.
* CDS turned into pseudo (interupted by a gap or chromosome end): NCAS_0D04980.
* CDS turned into pseudo (interupted by a gap or chromosome end): NCAS_0E04230.
* CDS turned into pseudo (interupted by a gap or chromosome end): NCAS_0H03380.
* Delete 54 spurious inference qualifiers in CDSs.

## v1.0 (2021-05-17)

### Added

* The 10 annotated chromosomes of Naumovozyma castellii CBS 4309 (source EBI, [GCA_000237345.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000237345.1)).
