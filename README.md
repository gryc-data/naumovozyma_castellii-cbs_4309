## *Naumovozyma castellii* CBS 4309

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJEA70625](https://www.ebi.ac.uk/ena/browser/view/PRJEA70625)
* **Assembly accession**: [GCA_000237345.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000237345.1)
* **Original submitter**: Wolfe Laboratory (Trinity College Bublin)

### Assembly overview

* **Assembly level**: Chromosome
* **Assembly name**: ASM23734v1
* **Assembly length**: 11,219,539
* **#Chromosomes**: 10
* **Mitochondiral**: No
* **N50 (L50)**: 1,245,273 (3)

### Annotation overview

* **Original annotator**: Wolfe Laboratory (Trinity College Bublin)
* **CDS count**: 5592
* **Pseudogene count**: 0
* **tRNA count**: 270
* **rRNA count**: 8
* **Mobile element count**: 0
